INSERT INTO ROLES (id, name) 
VALUES
  	(1, 'Manager');
  	
INSERT INTO EMPLOYEES (full_name, address, role_id, salary, dob) 
VALUES
  	('Elon Musk', 'South Africa', 1, 90000000, '1971-06-28T00:00:00Z');