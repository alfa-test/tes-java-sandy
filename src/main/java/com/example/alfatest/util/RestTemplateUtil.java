package com.example.alfatest.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.alfatest.constanta.DogUrlConstant;
import com.example.alfatest.exception.CustomRestTemplateException;
import com.example.alfatest.model.Dog;
import com.example.alfatest.module.dog.DogRedisRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestTemplateUtil {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private DogRedisRepository dogRedisRepository;
	
	public List<Dog> getListDog() {
		List<Dog> dogList = new ArrayList<Dog>();
      	Map<String, Object> json = new HashMap<>();
      	
      	try {
      		json = restTemplate.getForObject(DogUrlConstant.URL_GET_ALL, Map.class);
		} catch (Exception e) {
			throw new CustomRestTemplateException(e.getMessage());
		}
      	
      	if (json.containsKey("message")) {
      		ObjectMapper objectMapper = new ObjectMapper();
      		Map<String, List<Object>> messages = objectMapper.convertValue(json.get("message"), Map.class);
      		
      		for (Entry<String, List<Object>> message : messages.entrySet()) {
      			String breed = message.getKey();
      			List<Object> subBreeds = message.getValue();
      			
      			Dog dog = new Dog();
      			dog.setBreed(breed);
      			
      			List<Dog> subBreadList = new ArrayList<Dog>();
      			
      			for (Object subBreed : subBreeds) {
      				Dog subBreadDog = new Dog();
      				subBreadDog.setBreed(subBreed.toString());
      				subBreadDog.setSubBreed(new ArrayList<Dog>());
      				subBreadList.add(subBreadDog);
				}
      			
      			dog.setSubBreed(subBreadList);
      			dogRedisRepository.save(dog);
      			
      			dogList.add(dog);
			}
      	}
      	
		return dogList;
	}

	public Dog getDetailDog(String breed) {
		String urlSubBreed = DogUrlConstant.getSubBreadUrlByBreed(breed);
		
		Map<String, Object> jsonSubBreed = new HashMap<>();
		
		try {
			jsonSubBreed = restTemplate.getForObject(urlSubBreed, Map.class);
		} catch (Exception e) {
			throw new CustomRestTemplateException(e.getMessage());
		}
		
		Dog dog = new Dog();
		
		if (jsonSubBreed.containsKey("message")) {
			ObjectMapper objectMapper = new ObjectMapper();
			List<String> messages = objectMapper.convertValue(jsonSubBreed.get("message"), List.class);
			
			dog.setBreed(breed);
  			
  			List<Dog> subBreadList = new ArrayList<Dog>();
			
			for (String message : messages) {
				Dog subBreadDog= new Dog();
				subBreadDog.setBreed(message);
				subBreadDog.setSubBreed(new ArrayList<Dog>());
				subBreadList.add(subBreadDog);
			}
			
			dog.setSubBreed(subBreadList);
		}
		
		List<String> images = getImageByBreed(breed);
		dog.setImages(images);
		
		return dog;
	}
	
	public List<String> getImageByBreed(String breed) {
		List<String> images = new ArrayList<String>();
		Map<String, List<String>> jsonImages = new HashMap<>();
		
		String urlImages = DogUrlConstant.getImageUrlByBreed(breed);
		
		try {
			jsonImages = restTemplate.getForObject(urlImages, Map.class);
		} catch (Exception e) {
			throw new CustomRestTemplateException(e.getMessage());
		}
		
		if (jsonImages.containsKey("message")) {
			ObjectMapper objectMapper = new ObjectMapper();
			images = objectMapper.convertValue(jsonImages.get("message"), List.class);
		}
		
		return images;
	}
}
