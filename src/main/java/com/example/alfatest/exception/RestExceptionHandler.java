package com.example.alfatest.exception;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.alfatest.base.BaseResponseMessage;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {		
		
		String errorMessage = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
		BaseResponseMessage baseResponse = new BaseResponseMessage();
		baseResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
		baseResponse.setMessage(HttpStatus.BAD_REQUEST.name());
		baseResponse.setErrorMessage(errorMessage);
		
		return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(EntityNotFoundException.class)
	protected ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
		
		String errorMessage = ex.getMessage();
		BaseResponseMessage baseResponse = new BaseResponseMessage();
		baseResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
		baseResponse.setMessage(HttpStatus.BAD_REQUEST.name());
		baseResponse.setErrorMessage(errorMessage);
		
	    return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler({CustomRedisException.class, CustomRestTemplateException.class})
	protected ResponseEntity<?> handleRedisdException(CustomRedisException ex, WebRequest request) {
		
		String errorMessage = ex.getMessage();
		BaseResponseMessage baseResponse = new BaseResponseMessage();
		baseResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		baseResponse.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		baseResponse.setErrorMessage(errorMessage);
		
	    return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
