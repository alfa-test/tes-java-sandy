package com.example.alfatest.exception;

public class CustomRestTemplateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CustomRestTemplateException(String message) {
		super(message);
	}
}
