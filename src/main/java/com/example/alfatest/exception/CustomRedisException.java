package com.example.alfatest.exception;

public class CustomRedisException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public CustomRedisException(String message) {
		super(message);
	}
}
