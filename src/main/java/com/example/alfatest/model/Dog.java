package com.example.alfatest.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Dog implements Serializable {

	private static final long serialVersionUID = 1L;

	private String breed;
	
	private List<Dog> subBreed;
	
	private List<String> images;
}
