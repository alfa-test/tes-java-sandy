package com.example.alfatest.seed;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.alfatest.model.Employee;
import com.example.alfatest.model.Role;
import com.example.alfatest.module.employee.EmployeeRepository;
import com.example.alfatest.module.role.RoleRepository;

@Component
public class EmployeeSeeder {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private RoleRepository roleRepository;

	public void employeeSeederTable() {
		List<Employee> employees = employeeRepository.findAll();
		Optional<Role> role = roleRepository.findById(1L);
		
		if (employees.isEmpty()) {
			Employee employee = new Employee();
			employee.setFullName("Elon Musk");
			employee.setAddress("South Africa");
			
			if (role != null && role.isPresent()) {
				employee.setRole(role.get());
			}
			
			employee.setSalary(90000000L);
			employee.setDob("1971-06-28T00:00:00Z");
			employeeRepository.save(employee);
		}
	}
}
