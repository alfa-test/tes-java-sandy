package com.example.alfatest.seed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Seeder {
	
	@Autowired
	private RoleSeeder roleSeeder;
	
	@Autowired
	private EmployeeSeeder employeeSeeder;

	@EventListener
	public void seed(ContextRefreshedEvent event) {
		roleSeeder.roleSeederTable();
		employeeSeeder.employeeSeederTable();
	}
}
