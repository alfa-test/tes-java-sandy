package com.example.alfatest.seed;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.alfatest.model.Role;
import com.example.alfatest.module.role.RoleRepository;

@Component
public class RoleSeeder {
	
	@Autowired
	private RoleRepository roleRepository;

	public void roleSeederTable() {
		List<Role> roles = roleRepository.findAll();
		
		if (roles.isEmpty()) {
			Role role = new Role();
			role.setId(1L);
			role.setName("Manager");
			roleRepository.save(role);
		}
	}

}
