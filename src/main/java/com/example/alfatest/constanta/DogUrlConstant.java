package com.example.alfatest.constanta;

public class DogUrlConstant {

	public static final String URL_GET_ALL = "https://dog.ceo/api/breeds/list/all";
	private static final String URL_GET_BY_BREED = "https://dog.ceo/api/breed/";
	
	public static String getImageUrlByBreed(String breed) {
		String url = URL_GET_BY_BREED + breed + "/images";
		return url;
	}
	
	public static String getSubBreadUrlByBreed(String breed) {
		String url = URL_GET_BY_BREED + breed + "/list";
		return url;
	}
}
