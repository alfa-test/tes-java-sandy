package com.example.alfatest.base;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BaseResponseMessage {

	@JsonProperty("status_code")
	private Integer statusCode;
	
	private String message;
	
	@JsonProperty("error_message")
	private String errorMessage;
	
	@JsonProperty("success_message")
	private String successMessage;
}
