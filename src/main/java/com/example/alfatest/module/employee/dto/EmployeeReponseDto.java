package com.example.alfatest.module.employee.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EmployeeReponseDto {

	private Long id;
	
	@JsonProperty("full_name")
	private String fullName;
	
	private String address;
	
	private String dob;
	
	@JsonProperty("role_id")
	private Long roleId;
	
	@JsonProperty("role_name")
	private String roleName;
	
	private Long salary;
}
