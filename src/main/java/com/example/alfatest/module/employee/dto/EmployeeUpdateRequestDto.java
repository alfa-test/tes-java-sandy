package com.example.alfatest.module.employee.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EmployeeUpdateRequestDto {

	@JsonProperty("full_name")
	@NotBlank(message = "Full Name Cannot Be Null")
	private String fullName;
	
	@NotBlank(message = "Dob Cannot Be Null")
	private String dob;
	
	@NotNull(message = "Salary Cannot Be Null")
	private Long salary;
}
