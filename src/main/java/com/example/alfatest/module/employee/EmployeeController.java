package com.example.alfatest.module.employee;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.alfatest.base.BaseResponseMessage;
import com.example.alfatest.module.employee.dto.EmployeeReponseDto;
import com.example.alfatest.module.employee.dto.EmployeeUpdateRequestDto;
import com.example.alfatest.module.employee.dto.EmployeeInsertRequestDto;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@PostMapping
	public ResponseEntity<?> insertEmployee(@Valid @RequestBody EmployeeInsertRequestDto request) {
		EmployeeReponseDto response = employeeService.insertEmployee(request);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PutMapping(path = "/{id}")
	public ResponseEntity<?> updateEmployee(@Valid @PathVariable Long id, @RequestBody EmployeeUpdateRequestDto request) {
		EmployeeReponseDto response = employeeService.updateEmployee(id, request);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping
	public ResponseEntity<?> getListEmployee() {
		List<EmployeeReponseDto> response = employeeService.getListEmployee();
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<?> getEmployeeById(@PathVariable Long id) {
		EmployeeReponseDto response = employeeService.getEmployeeById(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> deleteEmployeeById(@PathVariable Long id) {
		BaseResponseMessage response = employeeService.deleteEmployeeById(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
