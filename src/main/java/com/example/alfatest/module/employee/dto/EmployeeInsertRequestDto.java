package com.example.alfatest.module.employee.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EmployeeInsertRequestDto {

	@JsonProperty("full_name")
	@NotBlank(message = "Full Name Cannot Be Null")
	private String fullName;
	
	@NotBlank(message = "Address Cannot Be Null")
	private String address;
	
	@NotBlank(message = "Dob Cannot Be Null")
	private String dob;
	
	@JsonProperty("role_id")
	@NotNull(message = "Role Id Cannot Be Null")
	@Min(value = 0, message = "Role Id Cannot Be Null")
	private Long roleId;
	
	@NotNull(message = "Salary Cannot Be Null")
	@Min(value = 0, message = "Salary Value Min 0")
	private Long salary;
}
