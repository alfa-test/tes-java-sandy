package com.example.alfatest.module.employee;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.alfatest.base.BaseResponseMessage;
import com.example.alfatest.model.Employee;
import com.example.alfatest.model.Role;
import com.example.alfatest.module.employee.dto.EmployeeReponseDto;
import com.example.alfatest.module.employee.dto.EmployeeUpdateRequestDto;
import com.example.alfatest.module.employee.dto.EmployeeInsertRequestDto;
import com.example.alfatest.module.role.RoleRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public EmployeeReponseDto insertEmployee(EmployeeInsertRequestDto request) {
		Long roleId = request.getRoleId();
		
		Role role = roleRepository.findById(roleId)
				.orElseThrow(() -> new EntityNotFoundException("Role Not Found"));
		
		Employee employee = modelMapper.map(request, Employee.class);
		employee.setId(null);
		employee.setRole(role);
		Employee newEmployee = employeeRepository.save(employee);
		
		EmployeeReponseDto employeeReponseDto = modelMapper.map(newEmployee, EmployeeReponseDto.class);
		employeeReponseDto.setRoleName(role.getName());
		
		return employeeReponseDto;
	}
	
	public EmployeeReponseDto updateEmployee(Long id, EmployeeUpdateRequestDto request) {
		String fullName = request.getFullName();
		Long salary = request.getSalary();
		String dob = request.getDob();
		
		Employee employee = employeeRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Employee Not Found"));
		
		Role employeeRole = employee.getRole();
		Role role = null;
		
		if (employeeRole != null) {
			role = roleRepository.findById(employee.getRole().getId())
					.orElseThrow(() -> new EntityNotFoundException("Role Not Found"));
		}
		
		employee.setFullName(fullName);
		employee.setSalary(salary);
		employee.setDob(dob);
		Employee updateEmployee = employeeRepository.save(employee);
		
		EmployeeReponseDto employeeReponseDto = modelMapper.map(updateEmployee, EmployeeReponseDto.class);
		
		if (role != null) {
			employeeReponseDto.setRoleName(role.getName());
		}
		
		return employeeReponseDto;
	}

	public List<EmployeeReponseDto> getListEmployee() {
		
		List<Employee> employees = employeeRepository.findAll();
		
		List<EmployeeReponseDto> employeeReponseDtos = employees.stream().map(a -> {
			EmployeeReponseDto employeeReponseDto = modelMapper.map(a, EmployeeReponseDto.class);
			Role role = a.getRole();
			
			if (role != null) {
				employeeReponseDto.setRoleName(role.getName());
			}
			
			return employeeReponseDto;
		}).collect(Collectors.toList());
		
		return employeeReponseDtos;
	}
	
	public EmployeeReponseDto getEmployeeById(Long id) {
		Employee employee = employeeRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Employee Not Found"));
		
		EmployeeReponseDto employeeReponseDto = modelMapper.map(employee, EmployeeReponseDto.class);
		Role role = employee.getRole();
		
		if (role != null) {
			employeeReponseDto.setRoleName(role.getName());
		}
				
		return employeeReponseDto;
	}

	public BaseResponseMessage deleteEmployeeById(Long id) {
		Employee employee = employeeRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Employee Not Found"));
		
		employeeRepository.delete(employee);
		
		BaseResponseMessage baseResponseMessage = new BaseResponseMessage();
		baseResponseMessage.setStatusCode(HttpStatus.OK.value());
		baseResponseMessage.setMessage(HttpStatus.OK.name());
		baseResponseMessage.setSuccessMessage("Employee Delete Success");
		
		return baseResponseMessage;
	}
}
