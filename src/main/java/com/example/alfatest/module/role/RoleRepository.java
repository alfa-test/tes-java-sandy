package com.example.alfatest.module.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.alfatest.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

}
