package com.example.alfatest.module.dog;

import java.util.Map;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.example.alfatest.model.Dog;

@Repository
public class DogRedisRepository {

	private HashOperations hashOperations;

    private RedisTemplate redisTemplate;
    
    public DogRedisRepository(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        hashOperations = redisTemplate.opsForHash();
    }
    
    public void save(Dog dog) {
        hashOperations.put("DOG", dog.getBreed(), dog);
    }
    
    public Map<String, Dog> findAll() {
        return hashOperations.entries("DOG");
    }
    
    public Dog findById(String breed) {
        return (Dog) hashOperations.get("DOG", breed);
    }
    
    public void update(Dog dog) {
        save(dog);
      }
}
