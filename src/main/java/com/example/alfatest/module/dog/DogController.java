package com.example.alfatest.module.dog;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.alfatest.module.dog.dto.DogResponseDto;

@RestController
@RequestMapping("/dogs")
public class DogController {
	
	@Autowired
	private DogService dogService;

	@GetMapping
	public ResponseEntity<?> getListDogs() {
		List<DogResponseDto> response = dogService.getListDogs();
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping("/{breed}")
	public ResponseEntity<?> getDetailDog(@PathVariable String breed) {
		DogResponseDto response = dogService.getDetailDog(breed);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
