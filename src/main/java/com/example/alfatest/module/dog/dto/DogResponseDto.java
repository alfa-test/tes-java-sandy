package com.example.alfatest.module.dog.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DogResponseDto {

	private String breed;
	
	@JsonProperty("sub_breed")
	private List<DogResponseDto> subBreed;
	
	@JsonInclude(Include.NON_NULL)
	private List<String> images;
}
