package com.example.alfatest.module.dog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.alfatest.exception.CustomRedisException;
import com.example.alfatest.model.Dog;
import com.example.alfatest.module.dog.dto.DogResponseDto;
import com.example.alfatest.util.RestTemplateUtil;

@Service
public class DogService {
	
	@Autowired
	private RestTemplateUtil restTemplateUtil;
	
	@Autowired
	private DogRedisRepository dogRedisRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	public List<DogResponseDto> getListDogs() {
		Map<String, Dog> dogs = new HashMap<>();
		
		try {
			dogs = dogRedisRepository.findAll();
		} catch (Exception e) {
			throw new CustomRedisException(e.getMessage());
		}
		
		
		List<DogResponseDto> dogResponseDtos = new ArrayList<DogResponseDto>();
		
		if (dogs.isEmpty()) {
			List<Dog> dogList = restTemplateUtil.getListDog();
			
			dogResponseDtos = dogList.stream().map(a -> {
				DogResponseDto dogResponseDto = modelMapper.map(a, DogResponseDto.class);
				return dogResponseDto;
			}).collect(Collectors.toList());
		} else {
			for (Entry<String, Dog> dog : dogs.entrySet()) {
				DogResponseDto dogResponseDto = modelMapper.map(dog.getValue(), DogResponseDto.class);
				dogResponseDtos.add(dogResponseDto);
			}
		}
		
		return dogResponseDtos;
	}

	public DogResponseDto getDetailDog(String breed) {
		Dog dog = null;
		
		try {
			dog = dogRedisRepository.findById(breed);
		} catch (Exception e) {
			throw new CustomRedisException(e.getMessage());
		}
		
		DogResponseDto dogResponseDto = new DogResponseDto();
		
		if (dog == null) {
			Dog dogRestTemplate = restTemplateUtil.getDetailDog(breed);
			dogResponseDto = modelMapper.map(dogRestTemplate, DogResponseDto.class);
		} else {
			dogResponseDto = modelMapper.map(dog, DogResponseDto.class);
			
			List<String> images = restTemplateUtil.getImageByBreed(breed);
			dogResponseDto.setImages(images);
		}
		
		return dogResponseDto;
	}
}
